MealwalkClient.PageOrderRoute = Ember.Route.extend
  model: ()->
    Em.RSVP.hash
      company: Em.Object.create()
      menus: @store.find('product', {ids:[2,3]})
      walk_parts: @store.find('product', {ids:[4,6,8,10]})
      currentMenu: @store.find('product', 2)

  setupController: (controller, context) ->
    for key in "company menus".w()
      @controllerFor(key).set 'content', context[key]
    controller.set('content', Em.Object.create(
      currentMenu: context['currentMenu']
      order: MealwalkClient.PageOrderOrder.create()
    ))