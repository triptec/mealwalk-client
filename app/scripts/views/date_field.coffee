MealwalkClient.DateField = Ember.TextField.extend(
  type: "date"
  date: ((key, date) ->
    if date
      @set "value", date
    else
      value = @get("value")
      if value
        date = new Date(value)
      else
        date = null
    date
  ).property("value")
)

MealwalkClient.DateTimeField = Ember.TextField.extend(
  type: "datetime-local"
  date: ((key, date) ->
    if date
      @set "value", date
    else
      value = @get("value")
      if value
        date = new Date(value)
      else
        date = null
    date
  ).property("value")
)

MealwalkClient.TimeField = Ember.TextField.extend(
  type: "time"
  date: ((key, date) ->
    if date
      @set "value", date
    else
      value = @get("value")
      if value
        date = new Date(value)
      else
        date = null
    date
  ).property("value")
)