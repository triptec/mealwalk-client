Placement = {
    initPeople: function(names)
    {
        people = [];
        for(i=0;i < names.length; i++){
            var person = {};
            person.id = i;
            person.name = names[i];
            person.met = [];
            person.beenAt = [];
            people.push(person);
        }
        return people;
    },

    initResturants: function(resturantNames,tableNum)
    {
        resturants = [];
        for(i=0;i < resturantNames.length; i++){
            var resturant = {};
            resturant.name = resturantNames[i];
            resturant.tables = [];
            resturants.push(resturant);

        }
        j=0;
        for(i=0;i<tableNum;i++)
        {
            resturants[j].tables.push([])
            j=j<resturants.length-1?j+1:0;
        }
        return resturants;
    },

    addMet: function(people, resturants)
    {
        for(j=0; j<resturants.length;j++)
        {
            for(k=0; k<resturants[j].tables.length;k++)
            {
                for(l=0;l<resturants[j].tables[k].length;l++)
                {
                    people[resturants[j].tables[k][l]].met = people[resturants[j].tables[k][l]].met.concat(resturants[j].tables[k].filter(function(p){return  p != resturants[j].tables[k][l] }));
                }
            }
        }
        return people;
    },
    /*
     This function has a force that will ignore all constraints (met ppl)
     We should do something about prioritize new friends or new restaurant, also optimize new friends
     */
    placeUnplaceable: function(people, resturants, unplaceablePeople, force)
    {
        tables = [];
        unplaceable = []
        for(i=0;i<unplaceablePeople.length;i++)
        {
            foundPlace = false;
            for(j=0; j<resturants.length;j++)
            {
                //
                //if(unplaceablePeople[i].beenAt.indexOf(resturants[j].name) > -1) continue; //If uncommented would prioritize new restaurants
                for(k=0; k<resturants[j].tables.length;k++)
                {

                    if(resturants[j].tables[k].length > 3) continue;
                    haveMet = false;
                    for(l=0;l<resturants[j].tables[k].length;l++)
                    {
                        //console.log("Looking for person " + resturants[j].tables[k][l] + " among person " + people[i].name + " friends(" + people[i].met.join() + ")" + " who is at table " + k + " at " + resturants[j].name + " that has " + resturants[j].tables[k].join());
                        if(unplaceablePeople[i].met.indexOf(resturants[j].tables[k][l]) > -1)
                        {
                            haveMet = true;
                        }
                    }
                    /*
                     var table = {}
                     table.friends = haveMet;
                     table.resturant = j;
                     table.table = k;
                     tables.push(table);
                     */

                    if(!haveMet || force) //This will place even if already has friends
                    {
                        resturants[j].tables[k].push(unplaceablePeople[i].id);
                        people[unplaceablePeople[i].id].beenAt.push(resturants[j].name);
                        foundPlace = true
                        break;
                    }


                }
                if(foundPlace)
                    break;
            }
            if(!foundPlace)
                unplaceable.push(unplaceablePeople[i]);
        }
        //console.log(tables);

        people = this.addMet(people, resturants);
        console.log('unplaceable even if goes to same resturant',unplaceable)
        return [people,resturants,unplaceable];
    },
    placePeople: function(people, resturants)
    {
        unplaceable = []
        for(i=0;i<people.length;i++)
        {
            foundPlace = false;
            for(j=0; j<resturants.length;j++)
            {
                if(people[i].beenAt.indexOf(resturants[j].name) > -1) continue;
                for(k=0; k<resturants[j].tables.length;k++)
                {

                    if(resturants[j].tables[k].length > 3) continue;
                    haveMet = false;
                    for(l=0;l<resturants[j].tables[k].length;l++)
                    {
                        //console.log("Looking for person " + resturants[j].tables[k][l] + " among person " + people[i].name + " friends(" + people[i].met.join() + ")" + " who is at table " + k + " at " + resturants[j].name + " that has " + resturants[j].tables[k].join());
                        if(people[i].met.indexOf(resturants[j].tables[k][l]) > -1)
                        {
                            haveMet = true;
                            break;
                        }
                    }

                    if(!haveMet)
                    {
                        resturants[j].tables[k].push(people[i].id);
                        people[i].beenAt.push(resturants[j].name);
                        foundPlace = true
                        break;
                    }
                }
                if(foundPlace)
                    break;
            }
            if(!foundPlace)
                unplaceable.push(people[i]);
        }
        people = this.addMet(people, resturants);
        console.log('optimal unplaceable',unplaceable)
        if(unplaceable.length){
            arr = this.placeUnplaceable(people,resturants, unplaceable);
            people = arr[0];
            resturants = arr[1];
            unplaceable = arr[2];

            if(unplaceable.length > 0){
                //Force placement of unplaceable ppl
                arr = this.placeUnplaceable(people,resturants, unplaceable, true);
                people = arr[0];
                resturants = arr[1];
            }
        }
        return [people,resturants];
    },
    colors: [
    "#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
    "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
    "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
    "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
    "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
    "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
    "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
    "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
    "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
    "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
    "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
    "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
    "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
    "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
    "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
    "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"
    ]
}