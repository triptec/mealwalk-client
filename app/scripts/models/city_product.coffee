MealwalkClient.CityProduct = DS.Model.extend(
  city: DS.belongsTo('city')
  product: DS.belongsTo('product')
  price: DS.attr()
)