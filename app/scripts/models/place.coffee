MealwalkClient.Place = DS.Model.extend(
  name: DS.attr()
  description: DS.attr()
  address: DS.attr()
  billing: DS.attr()
  contact: DS.attr()
  phone: DS.attr()
  email: DS.attr()
  products: DS.hasMany('product', { async: true })
)