MealwalkClient.Company = DS.Model.extend(
  name: DS.attr()
  reference_name: DS.attr()
  phone: DS.attr()
  email: DS.attr()
)