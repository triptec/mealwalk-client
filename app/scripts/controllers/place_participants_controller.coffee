MealwalkClient.PlaceParticipantsController = Ember.ObjectController.extend(
  participantsCount: 12
  enrich: (course, restaurants, ppl) ->
    name: course
    restaurants: restaurants.map (restaurant) ->
      name: restaurant.name
      tables: restaurant.tables.map (table) ->
        seats: table.map (seat) ->
          {id: seat,name:ppl[seat].name, color: Placement.colors[parseInt(seat)]}

  courses: (->
    count = @get('participantsCount')
    names = [1..count].map () ->
      Faker.Name.findName()
    people = []
    people = Placement.initPeople(names)
    console.log people
    tableNum = Math.ceil(people.length/4)
    resturantNames = ['mesoyo', 'opus', 'oskar']
    appetizer = Placement.placePeople(people, Placement.initResturants(resturantNames,tableNum))
    maincourse = Placement.placePeople(appetizer[0], Placement.initResturants(resturantNames,tableNum))
    dessert = Placement.placePeople(maincourse[0], Placement.initResturants(resturantNames,tableNum))
    people = dessert[0]
    data = for course, restaurants of {Appetizer: appetizer[1], Maincourse: maincourse[1], Dessert: dessert[1]}
      @enrich(course, restaurants, people)
    data
  ).property('participantsCount')
)
