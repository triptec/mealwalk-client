

MealwalkClient.PageOrderController = Ember.ObjectController.extend(
  needs: ['menus', 'company', 'walk_parts','accessory']
  minDate: moment().add('days', 7).format("YYYY-MM-DD")
  maxDate: moment().add('months', 6).format("YYYY-MM-DD")
  startDate: moment().add('days', 7).format("YYYY-MM-DD")

  minTime: "17:00"
  maxTime: "20:00"
  startTime: "18:00"

  city: "1"

  currentMenuChanged: (->
    @set('controllers.walk_parts.content', @get('currentMenu.child_products'))
  ).observes('currentMenu')

  menuInfo: (->
    return unless @get('currentMenu')
    @get('currentMenu.city_products').find(
      (item)=>
        item.get('city.id') == @get('city')
    )
  ).property('currentMenu.city_products.@each', 'city')

  menuPrice: Em.computed 'menuInfo', ->
    return 0 unless @get('menuInfo')
    @get('menuInfo.price')

  personTotal: (->
    @get('controllers.walk_parts.subTotal') + @get('menuPrice')
  ).property('controllers.walk_parts.subTotal', 'menuPrice')

  total: (->
    @get('personTotal') * @get('order.participants')
  ).property('personTotal', 'order.participants')

  actions:
    save: ()->
      #console.log @get('controllers.accessory')
      log(@get('order'))
)

MealwalkClient.PageOrderOrder = Em.Object.extend
  order_items: []
  participants: 12
  updateOrAdd: (item) ->
    updated = false
    @get('order_items').forEach (aItem, index, arr) ->
      unless updated
        parent_product = item.get('parent_product')
        id = item.get('id')
        if aItem.get('id') == id && aItem.get('parent_product') == parent_product
          aItem.setProperties(item.getProperties('id', 'parent_product', 'quantity', 'price'))
          updated = true
    unless updated
      @get('order_items').pushObject(item)

MealwalkClient.MenusController = Em.ArrayController.extend()
MealwalkClient.CompanyController = Em.ObjectController.extend()
MealwalkClient.ProductsController = Em.ArrayController.extend()
MealwalkClient.OrderController = Em.ObjectController.extend()


MealwalkClient.WalkPartsController = Em.ArrayController.extend
  itemController: 'walk_part'
  subTotal: Em.computed.sum('@each.subTotal')

MealwalkClient.WalkPartController = Em.ObjectController.extend
  needs: ['page_order']
  accessoriesTotal: 0
  info: (->
    @get('city_products').find(
      (item)=>
        item.get('city.id') == @get('controllers.page_order.city')
    )
  ).property('city_products.@each.city', 'controllers.page_order.city')

  subTotal: (->
    @get('accessoriesTotal') + @get('info.price')
  ).property('accessoriesTotal', 'info.price')


MealwalkClient.AccessoriesController = Em.ArrayController.extend
  itemController: 'accessory'
  subTotal: (->
    total = @_subControllers.reduce(
      (previousValue, item, index, enumerable)->
        return previousValue + item.get('subTotal')
      , 0)
    @set('parentController.accessoriesTotal', total)
    total
  ).property('@each.subTotal')

MealwalkClient.AccessoryController = Em.ObjectController.extend
  needs: ['page_order', 'accessories']
  quantity: 0
  info: (->
    @get('city_products').find(
      (item)=>
        item.get('city.id') == @get('controllers.page_order.city')
    )
  ).property('city_products.@each.city', 'controllers.page_order.city')
  subTotal: Em.computed 'info', 'quantity', ->
    @get('info.price') * parseInt(@get('quantity'))

  quantityChanged: (->
    #console.log @get('id'), @get('parentController.content.content.owner.id')
    if @get('quantity') > 0
      @get('controllers.page_order.order').updateOrAdd(Em.Object.create({id: @get('id'), parent_product: @get('parentController.content.content.owner.id') ,quantity: @get('quantity'), price: @get('info.price')}))
  ).observes('quantity')