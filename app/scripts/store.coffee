MealwalkClient.ApplicationAdapter = DS.ActiveModelAdapter.extend({
  host: 'http://mealwalkv2.herokuapp.com/'
  #host: 'http://mealwalkv2.dev/'
  buildURL: (record, suffix)->
    return @_super(record, suffix) + ".json"
})